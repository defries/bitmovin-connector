jQuery(document).ready(function ($) {

	Bitmovin = {

		ajax_create_manifests: true,

		init: function () {

			console.log('Bitmovin initialized');

			//initialize by add a click handler to our submit btn
			$('#bitmovin-submit').on('click', {mod: this}, this.submit_clicked);

			//or try to resume fetching updates...
			$( '#proceed-status-updates' ).each( function() {
				Bitmovin.update_status();
			});
		},

		submit_clicked: function (e) {
			e.preventDefault();

			//scope
			var mod = e.data.mod;

			console.log('Bitmovin submit was clicked');

			//aks the client if they have saved the page before doing this...
			if (confirm(bitmovin_params.confirm)) {

				//submit request when 'OK' was clicked
				mod.submit($(this).data('id'));
			}

			//stop default WP behavior
			return false;
		},

		submit: function (id) {

			var mod = this;

			//show feedback to the metabox
			$('#bitmovin-feedback').html('');
			mod.feedback('<p>' + bitmovin_params.submitted + '</p>');

			console.log('Bitmovin submitting: Encoding job');

			//disable btn
			$('#bitmovin-submit').prop('disabled', true);

			//submit to server
			$.ajax(
				{
					url: ajaxurl,
					dataType: 'JSON',
					method: 'POST',
					data: {
						'action': 'bitmovin_start_encoding_job',
						'post_id': id,
					},
				}
			).done(
				mod.submit_success
			);
		},

		submit_success: function (msg) {
			console.log('AJAX done');
			console.log(msg);

			if (msg.status == 'success') {

				//hide button if submit was a succes
				$('#bitmovin-submit').hide();
				//set a timer for our periodic updates
				Bitmovin.set_timer();
			} else {
				//re-enable our submit button, we might want to try this again
				$('#bitmovin-submit').prop('disabled', false);
			}

			$('#bitmovin-feedback').html('');
			for (i in msg.feedback) {
				Bitmovin.feedback(msg.feedback[i]);
			}

		},

		feedback: function (text) {
			//add a feedback line to the metabox.
			$('#bitmovin-feedback').append(text);
		},

		set_timer: function () {
			console.log('Setting timer for delayed update');
			//run update function after a 5sec wait
			setTimeout(Bitmovin.update_status, 5000);
		},

		update_status: function () {

			console.log('Doing server update of status');

			//scope
			var mod = this;

			//get the ID of this post
			var id = $('#bitmovin-submit').data('id');

			//send update request to server
			$.ajax(
				{
					url: ajaxurl,
					dataType: 'JSON',
					method: 'POST',
					data: {
						'action': 'bitmovin_update_encoding_status',
						'post_id': id,
					},
				}
			).done(
				Bitmovin.update_success
			);
		},

		update_success: function (msg) {
			console.log('Update:');
			console.log(msg);

			//if we are not finished yet, repeat the process of getting the status update
			if (msg.finished == false) {
				Bitmovin.set_timer();
			}

			//did we have an error? Then show the submit btn again, so we can try again
			if (msg.status == 'error') {
				$('#bitmovin-submit').prop('disabled', false).show();
			}

			if (Bitmovin.ajax_create_manifests) {

				if (( msg.status == 'success' ) && (msg.finished == true)) {
					//go and create the manifests
					Bitmovin.create_manifests();
				}
			}

			//show feedback to metabox
			$('#bitmovin-feedback').html('');

			//do we have extra messages yet?
			if ( msg.messages ) {
				for (i in msg.messages) {

					var line = '<p class="bitmovin-' + msg.messages[i]['type']  + '">' + msg.messages[i]['date'] + ' => ' + msg.messages[i]['text'] + '</p>';
					Bitmovin.feedback( line );
				}
			} else { //or shall we show the ole feedbacklines we created ourselves...
				for (i in msg.feedback) {
					Bitmovin.feedback(msg.feedback[i]);
				}
			}
		},

		create_manifests: function () {
			console.log('Creating manifests');

			//scope
			var mod = this;

			//get the ID of this post
			var id = $('#bitmovin-submit').data('id');

			//send update request to server
			$.ajax(
				{
					url: ajaxurl,
					dataType: 'JSON',
					method: 'POST',
					data: {
						'action': 'bitmovin_create_manifests',
						'post_id': id,
					},
				}
			).done(
				Bitmovin.manifests_creation_success
			);
		},

		manifests_creation_success: function (msg) {
			console.log('Manifests created:');
			console.log(msg);

			//show feedback to metabox
			$('#bitmovin-feedback').html('');
			for (i in msg.feedback) {
				Bitmovin.feedback(msg.feedback[i]);
			}
		}
	}

	//start the app
	Bitmovin.init();

});