<?php
/*
	Plugin Name: Bitmovin Connector
	Plugin URI: http://www.clubvirtual.com
	Description: Connects to Bitmovin, collects all the video data for use inside the CPT: Video's
	Author: Forsite Media
	Version: 0.1
	Author URI: http://www.forsite.media/
*/

define( 'BC_DIRNAME', dirname( plugin_basename( __FILE__ ) ) );
define( 'BC_PLUGIN_PATH', plugin_dir_path( __FILE__ ) );
define( 'BC_CLASS_PATH', BC_PLUGIN_PATH . '/classes/' );
define( 'BC_LANGUAGE_PATH', BC_DIRNAME . '/languages' ); ///needs to be relative to the plugins-base-dir
define( 'BC_PLUGIN_URI', WP_PLUGIN_URL . '/' . BC_DIRNAME );

//define which site we want to use to control the encodings
define( 'BC_ENCODING_BLOG_ID', 1 );

//Load Bitmovin
require_once __DIR__ . '/vendor/autoload.php';

/**
 * Read the classes directory and include all php-files inside by 'require_once'.
 */
foreach ( glob( BC_CLASS_PATH . '*.php' ) as $file ) {
	require_once( $file );
}

//if it exists, create it!
if ( class_exists( 'Bitmovin_Connector' ) ) {
	new Bitmovin_Connector();
}

register_deactivation_hook( __FILE__, 'bc_deactivation' );
/* Clear cron upon deactivation */
function bc_deactivation() {
	wp_clear_scheduled_hook( 'bc_update_event' );
}