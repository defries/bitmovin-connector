<?php

use Bitmovin\api\ApiClient;
use Bitmovin\api\model\webhooks\Webhook;
use Aws\S3\S3Client;

if ( ! class_exists( 'BC_Cron_Tasks' ) ) {

	class BC_Cron_Tasks {

		public function __construct() {

			//actions
			add_action( 'bc_update_event', array( $this, 'update' ) );
			add_action( 'bc_init_webhooks', array( $this, 'init_webhooks' ) );
			add_action( 'bc_unset_webhooks', array( $this, 'unset_webhooks' ) );
		}

		public function unset_webhooks() {

			$bitmovin_webhooks = get_site_option( 'bitmovin_webhooks' );

			if ( is_array( $bitmovin_webhooks ) ) {

				//get the specs
				$bitmovin_settings = get_site_option( 'bitmovin_settings' );
				$client            = new ApiClient( $bitmovin_settings['api_key_bitmovin'] );

				foreach ( $bitmovin_webhooks as $type => $bitmovin_webhook ) {

					$id = $bitmovin_webhook['id'];

					switch ( $type ) {
						case 'encoding-finished' :
							$detailsWebhook = $client->webhooks()->encodingFinished()->getById( $id );
							$client->webhooks()->encodingFinished()->delete( $detailsWebhook );
							break;
						case 'encoding-error' :
							$detailsWebhook = $client->webhooks()->encodingError()->getById( $id );
							$client->webhooks()->encodingError()->delete( $detailsWebhook );
							break;
						case 'transfer-finished' :
							$detailsWebhook = $client->webhooks()->transferFinished()->getById( $id );
							$client->webhooks()->transferFinished()->delete( $detailsWebhook );
							break;
						case 'transfer-error' :
							$detailsWebhook = $client->webhooks()->transferError()->getById( $id );
							$client->webhooks()->transferError()->delete( $detailsWebhook );
							break;
					}
				}
			}

			delete_site_option( 'bitmovin_webhooks' );

		}

		public function init_webhooks() {

			$bitmovin_webhooks = array();

			//create url's
/*
			$base_url = get_bloginfo( 'url' );
			$encoding_finished_url = add_query_arg( 'bitmovin', 'encoding-finished', $base_url );
			$encoding_error_url    = add_query_arg( 'bitmovin', 'encoding-error', $base_url );
			$transfer_finished_url = add_query_arg( 'bitmovin', 'transfer-finished', $base_url );
			$transfer_error_url    = add_query_arg( 'bitmovin', 'transfer-error', $base_url );
*/


			$base_url = 'https://requestb.in/10xbofv1';
			$encoding_finished_url = $base_url;
			$encoding_error_url    = $base_url;
			$transfer_finished_url = $base_url;
			$transfer_error_url    = $base_url;


			//get the specs
			$bitmovin_settings = get_site_option( 'bitmovin_settings' );
			$client            = new ApiClient( $bitmovin_settings['api_key_bitmovin'] );

			// Create encoding finished Webhook
			$webhook                                = new Webhook( $encoding_finished_url );
			$createdWebhook                         = $client->webhooks()->encodingFinished()->create( $webhook );
			$createdWebhookId                       = $createdWebhook->getId();
			$bitmovin_webhooks['encoding-finished'] = array(
				'url' => $encoding_finished_url,
				'id'  => $createdWebhookId,
			);

			// Create encoding error Webhook
			$webhook                             = new Webhook( $encoding_error_url );
			$createdWebhook                      = $client->webhooks()->encodingError()->create( $webhook );
			$createdWebhookId                    = $createdWebhook->getId();
			$bitmovin_webhooks['encoding-error'] = array(
				'url' => $encoding_error_url,
				'id'  => $createdWebhookId,
			);

			// Create transfer finished Webhook
			$webhook                                = new Webhook( $transfer_finished_url );
			$createdWebhook                         = $client->webhooks()->transferFinished()->create( $webhook );
			$createdWebhookId                       = $createdWebhook->getId();
			$bitmovin_webhooks['transfer-finished'] = array(
				'url' => $transfer_finished_url,
				'id'  => $createdWebhookId,
			);

			// Create transfer error Webhook
			$webhook                             = new Webhook( $transfer_error_url );
			$createdWebhook                      = $client->webhooks()->transferError()->create( $webhook );
			$createdWebhookId                    = $createdWebhook->getId();
			$bitmovin_webhooks['transfer-error'] = array(
				'url' => $transfer_error_url,
				'id'  => $createdWebhookId,
			);

			//store in site options
			update_site_option( 'bitmovin_webhooks', $bitmovin_webhooks );

		}

		//perform API request, update the JSON and sanitize all them custom post types
		public function update() {

			//get the specs
			$bitmovin_settings = get_site_option( 'bitmovin_settings' );

			$client = new ApiClient( $bitmovin_settings['api_key_bitmovin'] );

			$page     = 0;
			$pageSize = 25;
			$items    = $client->encodings()->listPage( $page * $pageSize, $pageSize );
  			foreach ( $items as $encoding ) {
				$status = $client->encodings()->status( $encoding )->getStatus();
			    $messages = $client->encodings()->status( $encoding )->getMessages();

				//writeln( "Found encoding with id '" . $encoding->getId() . "', name '" . $encoding->getName() . "', and state '$status" );
				//print_r_pre($messages);
				//$client->encodings()->delete($encoding);
			}

			//writeln( 'Bitmovin outputs' );
			//$bitmovin_outputs = get_site_option( 'bitmovin_outputs' );
			//print_r_pre( $bitmovin_outputs );

			//writeln( 'Bitmovin encoded_s3_inputs' );
			//$bitmovin_encoded_s3_inputs = get_site_option( 'bitmovin_encoded_s3_inputs' );
			//print_r_pre( $bitmovin_encoded_s3_inputs );


			//get all the input videos available on the S3 bucket
			$this->_get_inputs_from_s3();

			update_site_option( 'bitmovin_last_import', date( 'r' ) );
		}

		private function _get_inputs_from_s3() {

			//get the specs
			$bitmovin_settings = get_site_option( 'bitmovin_settings' );

			//create client to remote service
			$client = S3Client::factory( array(
				'key'    => $bitmovin_settings['api_key_amazon_s3'],
				'secret' => $bitmovin_settings['api_secret_amazon_s3'],
				'region' => $bitmovin_settings['bucket_region_amazon_s3'],
			) );

			$s3_inputs = array();

			// Use the high-level iterators (returns ALL of your objects).
			try {
				$objects = $client->getIterator( 'ListObjects', array(
					'Bucket' => $bitmovin_settings['bucket_name_amazon_s3'],
					'Prefix' => $bitmovin_settings['bucket_prefix_amazon_s3'],
				) );

				foreach ( $objects as $object ) {

					//skip the folder-name, we don't needz it
					if ( $object['Key'] != $bitmovin_settings['bucket_prefix_amazon_s3'] ) {
						$s3_inputs[ md5( $object['Key'] ) ] = $object['Key'];
					}
				}

			} catch ( S3Exception $e ) {
				//nothing here yet,...
			}

			update_site_option( 'bitmovin_s3_inputs', $s3_inputs );
		}
	}
}