<?php

if ( ! class_exists( 'BC_Webhooks' ) ) {

	class BC_Webhooks {

		private $_job = null;

		public function __construct() {

			//actions
			add_action( 'init', array( $this, 'init' ) );

			//filters
			add_filter( 'wp_mail_content_type', array( $this, 'set_content_type_html' ) );
		}

		public function set_content_type_html(){
			return 'text/html';
		}

		public function init() {

			//do we have a get request?
			if ( isset( $_GET['bitmovin'] ) && ( ! empty( $_GET['bitmovin'] ) ) ) {

				//sanitize the action from the GET-param
				$action = sanitize_title( $_GET['bitmovin'] );

				//perform a specific action according to the GET-param
				switch ( $action ) {
					case 'encoding-finished' :
						$this->_encoding_finished();
						break;
					case 'encoding-error' :
						$this->_encoding_error();
						break;
					case 'transfer-finished' :
						$this->_transfer_finished();
						break;
					case 'transfer-error' :
						$this->_transfer_error();
						break;
				}
			}
		}

		private function _encoding_finished() {
			$this->_resolve_encoding_job();
		}

		private function _encoding_error() {
			$this->_resolve_encoding_job();
		}

		private function _transfer_finished() {
			$this->_resolve_encoding_job();
		}

		private function _transfer_error() {
			$this->_resolve_encoding_job();
		}

		private function _resolve_encoding_job() {

			$message = '';

			$message .= '<p>Raw input</p>';
			$message .= '<pre>' . var_export( file_get_contents('php://input'), true ) . '</pre>';

			wp_mail( 'floris@forsite.media', 'Bitmovin webhook fired on: ' . get_bloginfo( 'url' ), $message );
		}
	}
}
