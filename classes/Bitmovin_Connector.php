<?php
/*
 * Main Bitmovin_Connector Class, initializes all needed subclasses and initializes the language file for translatations
 * 
 * @author Forsite Media
 */

if ( ! class_exists( 'Bitmovin_Connector' ) ) {

	class Bitmovin_Connector {

		public function __construct() {

			// Load languages on init
			add_action( 'init', array( $this, 'load_languagefile' ) );

			add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_scripts' ) );

			// Create a network admin page
			new BC_Network_Admin();

			// Add cron tasks
			new BC_Cron_Tasks();

			// Add webhook-listeners
			new BC_Webhooks();

			// Build all the necessary custom post types
			new BC_Custom_PostTypes();

			// Build all the necessary custom meta boxes for all post types
			new BC_Custom_Meta_Boxes();

			// Build all the necessary custom taxonomies for all post types
			new BC_Custom_Taxonomies();

			// Add some extra handy dandy functions or some function-functionality
			new BC_Custom_Functions();
		}

		public function enqueue_scripts( $hook ) {

			if ( 'post.php' != $hook ) {
				return;
			}

			// Register script
			$file    = 'assets/js/bitmovin-admin-app.js';
			$version = filemtime( trailingslashit( BC_PLUGIN_PATH ) . $file );
			wp_register_script( 'bitmovin-admin-app', trailingslashit( BC_PLUGIN_URI ) . $file, array( 'jquery' ),
				$version );

			// Localize script
			$bitmovin_params = array(
				'confirm' => __( 'Have you saved your settings? Otherwise your previously stored setting shall be used to create this encoding job.', 'bitmovin' ),
				'submitted' => __( 'Bitmoving encoding job has been submitted. You can now leave this page, or you can stay here and wait for status updates to appear here in 3,...2,...,1...', 'bitmovin' ),
			);
			wp_localize_script( 'bitmovin-admin-app', 'bitmovin_params', $bitmovin_params );

			// Enqueue script with localized data
			wp_enqueue_script( 'bitmovin-admin-app' );

		}

		public function load_languagefile() {
			load_plugin_textdomain( 'bitmovin', false, BC_LANGUAGE_PATH );
		}
	}
}
