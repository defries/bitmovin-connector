<?php

//include Bitmovin classes
use Bitmovin\BitmovinClient;

if ( ! class_exists( 'BC_Manifests' ) ) {

	class BC_Manifests {

		private $_post_id = null;
		private $_status = array();

		public function __construct( $post_id ) {

			//store post id
			$this->_post_id = $post_id;

			//start
			$this->_init();
		}

		private function _init() {

			//check for a proper post ID
			if ( ( ! isset( $this->_post_id ) ) || ( empty( $this->_post_id ) ) ) {
				$this->_status = array(
					'status'  => 'error',
					'message' => __( 'No post ID found, have you saved your settings?', 'bitmovin' ),
				);

				return;
			}

			//get the job-container from the post meta
			$job_container = get_post_meta( $this->_post_id, '_job-container', true );
			if ( empty( $job_container ) ) {

				$this->_status = array(
					'status'  => 'error',
					'message' => __( 'No encoding job found', 'bitmovin' ),
				);

				return;
			}

			//create manifests in Bitmovin and move them to the output folder
			$this->_create_manifests( $job_container );
		}

		private function _create_manifests( $job_container ) {

			//fetch our settings
			$bitmovin_settings = get_site_option( 'bitmovin_settings' );

			//create a Bitmovin client
			$client = new BitmovinClient( $bitmovin_settings['api_key_bitmovin'] );

			//retrieve settings, so we know which manifests to create
			$adaptive_streaming_formats = get_post_meta( $this->_post_id, '_adaptive_streaming_formats', true );

			//create manifest for mpeg-dash if applicable
			if ( in_array( 'mpeg-dash', $adaptive_streaming_formats ) ) {

				$client->createDashManifest( $job_container );

				$dash_url = $this->_resolve_manifest_uri( 'mpeg-dash' );

				update_post_meta( $this->_post_id, 'manifest-mpeg-dash', $dash_url );

				BC_Custom_Functions::store_feedback_line( $this->_post_id, 'MPEG-DASH Manifest created' );

			} else {
				$dash_url = false;
			}

			//create manifest for hls if applicable
			if ( in_array( 'hls', $adaptive_streaming_formats ) ) {

				$client->createHlsManifest( $job_container );

				$hls_url = $this->_resolve_manifest_uri( 'hls' );

				update_post_meta( $this->_post_id, 'manifest-hls', $hls_url );

				BC_Custom_Functions::store_feedback_line( $this->_post_id, 'HLS Manifest created' );

			} else {
				$hls_url = false;
			}

			$this->_append_to_output( $dash_url, $hls_url );

			//set status
			$this->_status['status']   = 'success';
			$this->_status['dash']     = $dash_url;
			$this->_status['hls']      = $hls_url;
			$this->_status['feedback'] = BC_Custom_Functions::get_feedback_lines( $this->_post_id );
		}

		private function _append_to_output( $dash_url, $hls_url ) {

			$bitmovin_outputs = get_site_option( 'bitmovin_outputs' );
			if ( ! $bitmovin_outputs || empty( $bitmovin_outputs ) ) {
				$bitmovin_outputs = array();
			}

			$bitmovin_encoded_s3_inputs = get_site_option( 'bitmovin_encoded_s3_inputs' );
			if ( ! $bitmovin_encoded_s3_inputs || empty( $bitmovin_encoded_s3_inputs ) ) {
				$bitmovin_encoded_s3_inputs = array();
			}

			//get the job ID
			$job_id = get_post_meta( $this->_post_id, '_job-id', true );

			//get the input key-value pairs
			$input_key       = get_post_meta( $this->_post_id, '_input', true );
			$bitmovin_inputs = get_site_option( 'bitmovin_s3_inputs' );
			$input_value     = $bitmovin_inputs[ $input_key ];

			//create a video object for all WP-sites in the network to use
			$video_object = array(
				'post-id'       => $this->_post_id,
				'post-name'     => get_the_title( $this->_post_id ),
				'input-key'     => $input_key,
				'input-name'    => $input_value,
				'hls-url'       => $hls_url,
				'mpeg-dash-url' => $dash_url
			);

			//store video object, overwriting the precious one, if any
			$bitmovin_outputs[ $job_id ] = $video_object;

			//store, so we know that this particular video input has been encoded
			if ( ! in_array( $input_key, $bitmovin_encoded_s3_inputs ) ) {
				$bitmovin_encoded_s3_inputs[] = $input_key;
			}

			//store it
			update_site_option( 'bitmovin_outputs', $bitmovin_outputs );
			update_site_option( 'bitmovin_encoded_s3_inputs', $bitmovin_encoded_s3_inputs );
		}

		private function _resolve_manifest_uri( $type ) {

			//fetch our settings
			$bitmovin_settings = get_site_option( 'bitmovin_settings' );

			//get the path of the original input on S3
			$filename_folder = get_post_meta( $this->_post_id, '_filename_folder', true );

			//define the proper extension
			switch ( $type ) {
				case 'hls' :
					$extension = 'm3u8';
					break;
				case 'mpeg-dash' :
				default :
					$extension = 'mpd';
					break;
			}

			///create URI
			return $bitmovin_settings['sftp_url'] . $bitmovin_settings['bucket_prefix_amazon_s3'] . $filename_folder . 'stream.' . $extension;
		}

		public function get_status() {
			return $this->_status;
		}
	}
}