<?php


//include Bitmovin classes
use Bitmovin\api\enum\CloudRegion;
use Bitmovin\api\enum\codecConfigurations\H264Profile;
use Bitmovin\api\enum\SelectionMode;
use Bitmovin\api\model\encodings\drms\cencSystems\CencMarlin;
use Bitmovin\BitmovinClient;
use Bitmovin\configs\audio\AudioStreamConfig;
use Bitmovin\configs\EncodingProfileConfig;
use Bitmovin\configs\JobConfig;
use Bitmovin\configs\manifest\DashOutputFormat;
use Bitmovin\configs\manifest\HlsConfigurationAudioVideoGroup;
use Bitmovin\configs\manifest\HlsOutputFormat;
use Bitmovin\configs\video\H264VideoStreamConfig;
use Bitmovin\input\S3Input;
use Bitmovin\output\FtpOutput;
use Bitmovin\output\SftpOutput;
use Bitmovin\configs\drm\cenc\CencPlayReady;
use Bitmovin\configs\drm\cenc\CencWidevine;
use Bitmovin\configs\drm\CencDrm;

if ( ! class_exists( 'BC_Encoding' ) ) {

	class BC_Encoding {

		private $_post_id = null;
		private $_status = array();

		private $_encoder_version = 'STABLE';

		public function __construct( $post_id ) {

			//store post id
			$this->_post_id = $post_id;

			//start
			$this->_init();
		}

		private function _init() {

			//check for a proper post ID
			if ( ( ! isset( $this->_post_id ) ) || ( empty( $this->_post_id ) ) ) {
				$this->_status = array(
					'status'  => 'error',
					'message' => __( 'No post ID found, have you saved your settings?', 'bitmovin' ),
				);

				return;
			}

			//get the name for this encoding
			$title = get_the_title( $this->_post_id );
			if ( empty( $title ) ) {
				$title = 'Encoding-' . $this->_post_id;
			}

			//get the input video file
			$input = get_post_meta( $this->_post_id, '_input', true );
			if ( empty( $input ) ) {

				$this->_status = array(
					'status'  => 'error',
					'message' => __( 'No input video', 'bitmovin' ),
				);

				return;
			}

			//get the encoding profiles for the video
			$video_profiles = get_post_meta( $this->_post_id, '_encoding_profiles_video', true );
			if ( empty( $video_profiles ) ) {
				$this->_status = array(
					'status'  => 'error',
					'message' => __( 'No video encoding profiles were found', 'bitmovin' ),
				);

				return;
			}

			//get the encoding profiles for the audio
			$audio_profiles = get_post_meta( $this->_post_id, '_encoding_profiles_audio', true );
			if ( empty( $audio_profiles ) ) {
				$this->_status = array(
					'status'  => 'error',
					'message' => __( 'No audio encoding profiles were found', 'bitmovin' ),
				);

				return;
			}

			//get the encoding profiles for the audio
			$formats = get_post_meta( $this->_post_id, '_adaptive_streaming_formats', true );
			if ( empty( $formats ) ) {
				$this->_status = array(
					'status'  => 'error',
					'message' => __( 'No adaptive streaming formats were found.', 'bitmovin' ),
				);

				return;
			}

			//cleanup old feedback from a previuous encoding job
			delete_post_meta( $this->_post_id, '_feedback' );

			//start creating a new encoding job
			$this->_create_job( $title, $input, $video_profiles, $audio_profiles, $formats );
		}

		private function _create_job( $title, $video_key, $video_profiles, $audio_profiles, $formats ) {

			//get settings from the db
			$bitmovin_settings  = get_site_option( 'bitmovin_settings' );
			$bitmovin_s3_inputs = get_site_option( 'bitmovin_s3_inputs' );

			//start by creating an encoding profile
			$encodingProfile                 = new EncodingProfileConfig();
			$encodingProfile->name           = $title;
			$encodingProfile->cloudRegion    = CloudRegion::GOOGLE_EUROPE_WEST_1;
			$encodingProfile->encoderVersion = $this->_encoder_version;

			//resolve video-key from the selectbox to a video filename
			$video_file = $bitmovin_s3_inputs[ $video_key ];
			$video_arr  = explode( '/', $video_file );

			$filename        = array_pop( $video_arr );
			$filename_arr    = explode( '.', $filename );
			$filename_folder = array_shift( $filename_arr );

			//every job needs an input file
			$input = new S3Input(
				$bitmovin_settings['api_key_amazon_s3'],
				$bitmovin_settings['api_secret_amazon_s3'],
				$bitmovin_settings['bucket_name_amazon_s3'],
				$video_file
			);

			//start creating collections
			$hi_res_videos = array();
			$lo_res_videos = array();

			$hi_res_audio = array();
			$lo_res_audio = array();

			//create video and audio configs
			if ( in_array( 'videoStreamConfig_1080', $video_profiles ) ) {
				$videoStreamConfig_1080                = new H264VideoStreamConfig();
				$videoStreamConfig_1080->input         = $input;
				$videoStreamConfig_1080->width         = 1920;
				$videoStreamConfig_1080->height        = 1080;
				$videoStreamConfig_1080->bitrate       = 4992000;
				$videoStreamConfig_1080->rate          = 25.0;
				$videoStreamConfig_1080->selectionMode = SelectionMode::VIDEO_RELATIVE;
				$encodingProfile->videoStreamConfigs[] = $videoStreamConfig_1080;

				//store
				$hi_res_videos[] = $videoStreamConfig_1080;
			}

			if ( in_array( 'videoStreamConfig_720_HQ', $video_profiles ) ) {
				$videoStreamConfig_720_HQ                = new H264VideoStreamConfig();
				$videoStreamConfig_720_HQ->input         = $input;
				$videoStreamConfig_720_HQ->width         = 1280;
				$videoStreamConfig_720_HQ->height        = 720;
				$videoStreamConfig_720_HQ->bitrate       = 3072000;
				$videoStreamConfig_720_HQ->rate          = 25.0;
				$videoStreamConfig_720_HQ->selectionMode = SelectionMode::VIDEO_RELATIVE;
				$encodingProfile->videoStreamConfigs[]   = $videoStreamConfig_720_HQ;

				//store
				$hi_res_videos[] = $videoStreamConfig_720_HQ;
			}

			if ( in_array( 'videoStreamConfig_720', $video_profiles ) ) {
				$videoStreamConfig_720                 = new H264VideoStreamConfig();
				$videoStreamConfig_720->input          = $input;
				$videoStreamConfig_720->width          = 1280;
				$videoStreamConfig_720->height         = 720;
				$videoStreamConfig_720->bitrate        = 2496000;
				$videoStreamConfig_720->rate           = 25.0;
				$videoStreamConfig_720->selectionMode  = SelectionMode::VIDEO_RELATIVE;
				$encodingProfile->videoStreamConfigs[] = $videoStreamConfig_720;

				//store
				$lo_res_videos[] = $videoStreamConfig_720;
			}

			if ( in_array( 'videoStreamConfig_576', $video_profiles ) ) {
				$videoStreamConfig_576                 = new H264VideoStreamConfig();
				$videoStreamConfig_576->input          = $input;
				$videoStreamConfig_576->width          = 1024;
				$videoStreamConfig_576->height         = 576;
				$videoStreamConfig_576->bitrate        = 1856000;
				$videoStreamConfig_576->rate           = 25.0;
				$videoStreamConfig_576->selectionMode  = SelectionMode::VIDEO_RELATIVE;
				$encodingProfile->videoStreamConfigs[] = $videoStreamConfig_576;

				//store
				$lo_res_videos[] = $videoStreamConfig_576;
			}

			if ( in_array( 'videoStreamConfig_480', $video_profiles ) ) {
				$videoStreamConfig_480                 = new H264VideoStreamConfig();
				$videoStreamConfig_480->input          = $input;
				$videoStreamConfig_480->width          = 848;
				$videoStreamConfig_480->height         = 480;
				$videoStreamConfig_480->bitrate        = 1216000;
				$videoStreamConfig_480->rate           = 25.0;
				$videoStreamConfig_480->selectionMode  = SelectionMode::VIDEO_RELATIVE;
				$encodingProfile->videoStreamConfigs[] = $videoStreamConfig_480;

				//store
				$lo_res_videos[] = $videoStreamConfig_480;
			}

			if ( in_array( 'videoStreamConfig_360', $video_profiles ) ) {
				$videoStreamConfig_360                 = new H264VideoStreamConfig();
				$videoStreamConfig_360->input          = $input;
				$videoStreamConfig_360->width          = 640;
				$videoStreamConfig_360->height         = 360;
				$videoStreamConfig_360->bitrate        = 896000;
				$videoStreamConfig_360->rate           = 25.0;
				$videoStreamConfig_360->profile        = H264Profile::BASELINE;
				$videoStreamConfig_360->selectionMode  = SelectionMode::VIDEO_RELATIVE;
				$encodingProfile->videoStreamConfigs[] = $videoStreamConfig_360;

				//store
				$lo_res_videos[] = $videoStreamConfig_360;
			}

			if ( in_array( 'videoStreamConfig_240', $video_profiles ) ) {
				$videoStreamConfig_240                 = new H264VideoStreamConfig();
				$videoStreamConfig_240->input          = $input;
				$videoStreamConfig_240->width          = 424;
				$videoStreamConfig_240->height         = 240;
				$videoStreamConfig_240->bitrate        = 576000;
				$videoStreamConfig_240->rate           = 25.0;
				$videoStreamConfig_240->profile        = H264Profile::BASELINE;
				$videoStreamConfig_240->selectionMode  = SelectionMode::VIDEO_RELATIVE;
				$encodingProfile->videoStreamConfigs[] = $videoStreamConfig_240;

				//store
				$lo_res_videos[] = $videoStreamConfig_240;
			}

			if ( in_array( 'audioConfig_128', $audio_profiles ) ) {
				$audioConfig_128                       = new AudioStreamConfig();
				$audioConfig_128->input                = $input;
				$audioConfig_128->bitrate              = 128000;
				$audioConfig_128->rate                 = 48000;
				$audioConfig_128->name                 = 'English';
				$audioConfig_128->lang                 = 'en';
				$audioConfig_128->selectionMode        = SelectionMode::AUDIO_RELATIVE;
				$encodingProfile->audioStreamConfigs[] = $audioConfig_128;

				//store
				$hi_res_audio[] = $audioConfig_128;
			}

			if ( in_array( 'audioConfig_64', $audio_profiles ) ) {
				$audioConfig_64                        = new AudioStreamConfig();
				$audioConfig_64->input                 = $input;
				$audioConfig_64->bitrate               = 64000;
				$audioConfig_64->rate                  = 48000;
				$audioConfig_64->name                  = 'English';
				$audioConfig_64->lang                  = 'en';
				$audioConfig_64->selectionMode         = SelectionMode::AUDIO_RELATIVE;
				$encodingProfile->audioStreamConfigs[] = $audioConfig_64;

				//store
				$lo_res_audio[] = $audioConfig_64;
			}

			// Then we create an output configuration
			$jobConfig                  = new JobConfig();
			$jobConfig->output          = new FtpOutput(
				trim( $bitmovin_settings['sftp_host'] ),
				trim( $bitmovin_settings['sftp_username'] ),
				trim( $bitmovin_settings['sftp_password'] ),
				trailingslashit( $bitmovin_settings['sftp_path'] ) . trailingslashit( $bitmovin_settings['bucket_prefix_amazon_s3'] . trailingslashit( $filename_folder ) ) //store using base-path, original-folder-structure, and original file-basename as a new path
			);
			$jobConfig->encodingProfile = $encodingProfile;

			//Add adaptive streaming format for MPEG-DASH
			if ( in_array( 'mpeg-dash', $formats ) ) {

				//create Dash Output format
				$outputFormat = new DashOutputFormat();

				//Add DRM to output
				//$outputFormat->cenc = new CencDrm( $bitmovin_settings['drm_key'], $bitmovin_settings['drm_kid'] );
				//$outputFormat->cenc->setWidevine( new CencWidevine( $bitmovin_settings['widevine_pssh'] ) );
				//$outputFormat->cenc->setPlayReady( new CencPlayReady( $bitmovin_settings['playready_laurl'] ) );
				//$outputFormat->cenc->setMarlin( new CencMarlin() );

				//append format to job
				$jobConfig->outputFormat[] = $outputFormat;
			}

			//Add adaptive streaming format for Apple HLS
			if ( in_array( 'hls', $formats ) ) {

				//Hi-speed or low-speed configurations
				$hlsConfiguration                         = new HlsOutputFormat();
				$lowQualityAudioVideoGroup                = new HlsConfigurationAudioVideoGroup();
				$lowQualityAudioVideoGroup->audioStreams  = $lo_res_audio;
				$lowQualityAudioVideoGroup->videoStreams  = $lo_res_videos;
				$hlsConfiguration->audioVideoGroups[]     = $lowQualityAudioVideoGroup;
				$highQualityAudioVideoGroup               = new HlsConfigurationAudioVideoGroup();
				$highQualityAudioVideoGroup->audioStreams = $hi_res_audio;
				$highQualityAudioVideoGroup->videoStreams = $hi_res_videos;
				$hlsConfiguration->audioVideoGroups[]     = $highQualityAudioVideoGroup;
				$jobConfig->outputFormat[]                = $hlsConfiguration;
			}

			//$message = '';
			//$message .= '<p>Raw jobConfig</p>';
			//$message .= '<pre>' . var_export( $jobConfig , true ) . '</pre>';

			//wp_mail( 'floris@forsite.media', 'Bitmovin encoding job started on: ' . get_bloginfo( 'url' ), $message );

			// Run job
			$client        = new BitmovinClient( $bitmovin_settings['api_key_bitmovin'] );
			$job_container = $client->startJob( $jobConfig );
			$encoding_ids  = $job_container->getEncodingIds();
			$encoding_id   = $encoding_ids[0]; // there can be only one...

			//save encoding IDs as post meta
			update_post_meta( $this->_post_id, '_job-encoding-ids', $encoding_ids ); //just to be an the asfe side
			update_post_meta( $this->_post_id, '_job-id', $encoding_id );

			//Save the entire job-container, so we can grab the status later on
			update_post_meta( $this->_post_id, '_job-container', $job_container );

			//Save filename folder
			update_post_meta( $this->_post_id, '_filename_folder', trailingslashit( $filename_folder ) );

			//set current status
			update_post_meta( $this->_post_id, '_encoding_status', 'CREATED' );
			BC_Custom_Functions::store_feedback_line( $this->_post_id, 'CREATED' );

			//$this->_status['job-container'] = $job_container;
			$this->_status['job-id']   = $encoding_id;
			$this->_status['status']   = 'success';
			$this->_status['feedback'] = BC_Custom_Functions::get_feedback_lines( $this->_post_id );
		}

		public function get_status() {
			return $this->_status;
		}
	}
}