<?php
/*
 * All extra custom functions go inside this class, use this for image-sizes, ajax-callbacks et cetera
 * 
 * @author Forsite Media
 */

use Bitmovin\api\ApiClient;

if ( ! class_exists( 'BC_Custom_Functions' ) ) {

	class BC_Custom_Functions {

		public function __construct() {

			//let AJAX know where our functions are
			add_action( 'wp_ajax_bitmovin_start_encoding_job', array( $this, 'start_encoding_job' ) );
			add_action( 'wp_ajax_bitmovin_update_encoding_status', array( $this, 'update_encoding_status' ) );
			add_action( 'wp_ajax_bitmovin_create_manifests', array( $this, 'create_manifests' ) );

			//show encoding status as admin column
			add_filter( 'manage_encoding_posts_columns', array( $this, 'add_encoding_columns' ) );
			add_action( 'manage_encoding_posts_custom_column', array( $this, 'endcoding_custom_columns' ), 10, 2 );

			//when some deletes an encoding...
			add_action( 'before_delete_post', array( $this, 'delete_encoding' ) );
		}


		public function delete_encoding( $post_id ) {

			// We check if the global post type isn't ours and just return
			global $post_type;
			if ( $post_type != 'encoding' ) {
				return;
			}

			//get the job id for the remote encoding
			$job_id = get_post_meta( $post_id, '_job-id', true );


			//now we loop through all the sites,...this may take a while...
			$sites = get_sites();
			foreach ( $sites as $site ) {
				switch_to_blog( $site->blog_id );

				// The args
				$args = array(
					'post_type'      => 'variant',
					'posts_per_page' => - 1,
					'fields'         => 'ids',
					'meta_query'     => array(
						'relation' => 'OR',
						array(
							'key'     => '_video_file_main',
							'value'   => $job_id,
							'compare' => '=',
						),
						array(
							'key'     => '_video_file_preview',
							'value'   => $job_id,
							'compare' => '=',
						),
					),
				);

				// The Query
				$the_query = new WP_Query( $args );

				foreach ( (array) $the_query->posts as $variant_id ) {

					//set post status to draft
					$variant = array(
						'ID'          => $variant_id,
						'post_status' => 'draft',
					);

					// Update the post into the database
					wp_update_post( $variant );
				}
			}

			//go back tot the original site we were on
			restore_current_blog();

			//get the current list of available outputs
			$bitmovin_outputs = get_site_option( 'bitmovin_outputs' );

			//retrieve the key for the original input
			if ( isset( $bitmovin_outputs[ $job_id ] ) ) {
				$output    = $bitmovin_outputs[ $job_id ];
				$input_key = $output['input-key'];

				//remove the item from the list of available outputs
				unset( $bitmovin_outputs[ $job_id ] );
				update_site_option( 'bitmovin_outputs', $bitmovin_outputs );
			}

			//get the list of the inputs we allready encoded, and remove this item from that list
			$bitmovin_encoded_s3_inputs = get_site_option( 'bitmovin_encoded_s3_inputs' );
			if ( isset( $input_key ) && isset( $bitmovin_encoded_s3_inputs[ $input_key ] ) ) {
				unset( $bitmovin_encoded_s3_inputs[ $input_key ] );
				update_site_option( 'bitmovin_encoded_s3_inputs', $bitmovin_encoded_s3_inputs );
			}

			try {
				//remove encoding from Bitmovin
				$bitmovin_settings = get_site_option( 'bitmovin_settings' );
				$client            = new ApiClient( $bitmovin_settings['api_key_bitmovin'] );
				$encoding          = $client->encodings()->getById( $job_id );
				$client->encodings()->delete( $encoding );
			} catch (Exception $e) {
				//echo 'Caught exception: ',  $e->getMessage(), "\n";
			}

		}

		public function add_encoding_columns( $columns ) {
			return array_merge( $columns,
				array( 'encoding-status' => __( 'Encoding status', 'bitmovin' ) ) );
		}

		public function endcoding_custom_columns( $column, $post_id ) {
			switch ( $column ) {

				case 'encoding-status' :
					echo get_post_meta( $post_id, '_encoding_status', true );
					break;
			}
		}

		public static function store_feedback_line( $post_id, $feedback ) {

			//store a line of feedback to the post meta
			$feedback = '<p>' . date( 'r' ) . ' => ' . $feedback . '</p>';
			add_post_meta( $post_id, '_feedback', $feedback, false );
		}

		public static function get_feedback_lines( $post_id ) {

			//get all stored feedbacklines
			$feedback = get_post_meta( $post_id, '_feedback' );

			return $feedback;
		}

		public function create_manifests() {

			//sanitize data
			$post_id = intval( $_POST['post_id'] );

			//get encoding job status
			$encoding_manifests = new BC_Manifests( $post_id );
			$status             = $encoding_manifests->get_status();

			//return to client
			wp_send_json( $status );
		}

		public function update_encoding_status() {

			//sanitize data
			$post_id = intval( $_POST['post_id'] );

			//get encoding job status
			$encoding_status = new BC_Status( $post_id );
			$status          = $encoding_status->get_status();

			//return to client
			wp_send_json( $status );
		}

		public function start_encoding_job() {

			//sanitize data
			$post_id = intval( $_POST['post_id'] );

			//start encoding job, get the status
			$encoding_job = new BC_Encoding( $post_id );
			$status       = $encoding_job->get_status();

			//return to client
			wp_send_json( $status );
		}

		public static function get_input_videos() {

			$inputs         = get_site_option( 'bitmovin_s3_inputs' );
			$encoded_inputs = get_site_option( 'bitmovin_encoded_s3_inputs' );

			if ( is_array( $encoded_inputs ) ) {
				foreach ( (array) $inputs as $key => $value ) {

					if ( in_array( $key, $encoded_inputs ) ) {
						$value .= __( ' => allready encoded', 'customgoodies' );
						$inputs[ $key ] = $value;
					}
				}
			}

			return $inputs;
		}

		public static function get_encoding_profiles_video() {

			$profiles = array(
				'videoStreamConfig_1080'   => '1080',
				'videoStreamConfig_720_HQ' => '720 HQ',
				'videoStreamConfig_720'    => '720',
				'videoStreamConfig_576'    => '576',
				'videoStreamConfig_480'    => '480',
				'videoStreamConfig_360'    => '360',
				'videoStreamConfig_240'    => '240',
			);

			return $profiles;
		}

		public static function get_encoding_profiles_audio() {

			$profiles = array(
				'audioConfig_128' => '128 bps',
				'audioConfig_64'  => '64 bps',
			);

			return $profiles;
		}

		public static function get_adaptive_streaming_formats() {

			$formats = array(
				'mpeg-dash' => 'MPEG-DASH',
				'hls'       => 'Apple HLS',
			);

			return $formats;
		}

		public static function get_available_manifests() {

			$manifests = array();

			//get all the available output info
			$bitmovin_outputs = get_site_option( 'bitmovin_outputs' );
			foreach ( (array) $bitmovin_outputs as $id => $info ) {
				//only return the id/name-pair
				$manifests[ $id ] = $info['post-name'];
			}

			return $manifests;
		}
	}
}