<?php

//include Bitmovin classes
use Bitmovin\BitmovinClient;
use Bitmovin\api\ApiClient;
use Bitmovin\api\enum\Status;

if ( ! class_exists( 'BC_Status' ) ) {

	class BC_Status {

		private $_post_id = null;
		private $_status = array();

		public function __construct( $post_id ) {

			//store post id
			$this->_post_id = $post_id;

			//start
			$this->_init();
		}

		private function _init() {

			//check for a proper post ID
			if ( ( ! isset( $this->_post_id ) ) || ( empty( $this->_post_id ) ) ) {
				$this->_status = array(
					'status'  => 'error',
					'message' => __( 'No post ID found, have you saved your settings?', 'bitmovin' ),
				);

				return;
			}

			//get the job-container from the post meta
			$job_container = get_post_meta( $this->_post_id, '_job-container', true );
			if ( empty( $job_container ) ) {

				$this->_status = array(
					'status'  => 'error',
					'message' => __( 'No encoding job found', 'bitmovin' ),
				);

				return;
			}

			$job_id = get_post_meta( $this->_post_id, '_job-id', true );
			if ( empty( $job_id ) ) {

				$this->_status = array(
					'status'  => 'error',
					'message' => __( 'No encoding job id found', 'bitmovin' ),
				);

				return;
			}

			//retrieve status from Bitmovin
			$this->_retrieve_remote_status( $job_container, $job_id );
		}

		private function _retrieve_remote_status( $job_container, $job_id ) {

			$status = 'success';

			//fetch our settings
			$bitmovin_settings = get_site_option( 'bitmovin_settings' );

			//create aBitmovin client
			$client     = new BitmovinClient( $bitmovin_settings['api_key_bitmovin'] );
			$api_client = new ApiClient( $bitmovin_settings['api_key_bitmovin'] );

			$allFinished = true;
			$client->updateEncodingJobStatus( $job_container );
			foreach ( $job_container->encodingContainers as $encodingContainer ) {

				// Append detailed status information
				BC_Custom_Functions::store_feedback_line( $this->_post_id, $encodingContainer->status );
				update_post_meta( $this->_post_id, '_encoding_status', $encodingContainer->status );
				if ( $encodingContainer->status != Status::FINISHED && $encodingContainer->status != Status::ERROR ) {
					$allFinished = false;
				}

				if ( $encodingContainer->status == Status::ERROR ) {
					$status = 'error';
				}

				//get the specific messages via another APIClient,...
				$items = $api_client->encodings()->listPage( 0, 25 );
				foreach ( $items as $encoding ) {
					if ( $encoding->getId() == $job_id ) {
						$this->_status['messages'] = $api_client->encodings()->status( $encoding )->getMessages();
						update_post_meta( $this->_post_id, '_messages', $this->_status['messages'] );
					}
				}
			}

			$this->_status['status']   = $status;
			$this->_status['finished'] = $allFinished;
			$this->_status['feedback'] = BC_Custom_Functions::get_feedback_lines( $this->_post_id );
		}

		public function get_status() {
			return $this->_status;
		}
	}
}