<?php
/*
 * All registrations for custom taxonomies, have them made by a generator like on themergency.com and paste the functions in here, and have them run from: register_all_customtaxonomies()
 * 
 * @author Forsite Media
 */

if ( ! class_exists( 'BC_Custom_Taxonomies' ) ) {

	class BC_Custom_Taxonomies {

		public function __construct() {
			add_action( 'init', array( $this, 'register_all_customtaxonomies' ) );
		}

		public function register_all_customtaxonomies() {
			$this->_register_taxonomy_variant_category();
			$this->_register_taxonomy_variant_intensity();
			$this->_register_taxonomy_variant_tag();
		}

		private function _register_taxonomy_variant_tag() {

			$singular = __( 'Tag', 'bitmovin' );
			$plural   = __( 'Tags', 'bitmovin' );

			$labels = array(
				'name'                       => $plural,
				'singular_name'              => $singular,
				'search_items'               => sprintf( __( 'Search %s', 'bitmovin' ), $plural ),
				'popular_items'              => sprintf( __( 'Popular %s', 'bitmovin' ), $plural ),
				'all_items'                  => sprintf( __( 'All %s', 'bitmovin' ), $plural ),
				'parent_item'                => sprintf( __( 'Parent %s', 'bitmovin' ), $singular ),
				'parent_item_colon'          => sprintf( __( 'Parent %s:', 'bitmovin' ), $singular ),
				'edit_item'                  => sprintf( __( 'Edit %s', 'bitmovin' ), $singular ),
				'update_item'                => sprintf( __( 'Update %s', 'bitmovin' ), $singular ),
				'add_new_item'               => sprintf( __( 'Add New %s', 'bitmovin' ), $singular ),
				'new_item_name'              => sprintf( __( 'New %s', 'bitmovin' ), $singular ),
				'separate_items_with_commas' => sprintf( __( 'Separate %s with commas', 'bitmovin' ), $plural ),
				'add_or_remove_items'        => sprintf( __( 'Add or remove %s', 'bitmovin' ), $plural ),
				'choose_from_most_used'      => sprintf( __( 'Choose from the most used %s', 'bitmovin' ), $plural ),
				'menu_name'                  => $plural,
			);

			$args = array(
				'labels'            => $labels,
				'public'            => true,
				'show_in_nav_menus' => false,
				'show_ui'           => true,
				'show_tagcloud'     => true,
				'show_admin_column' => true,
				'hierarchical'      => false,
				'rewrite'           => array( 'slug' => _x( 'variant-tag', 'single-slug', 'bitmovin' ) ),
				'query_var'         => true
			);

			register_taxonomy( 'variant-tag', array( 'variant' ), $args );
		}

		private function _register_taxonomy_variant_category() {

			$singular = __( 'Category', 'bitmovin' );
			$plural   = __( 'Categories', 'bitmovin' );

			$labels = array(
				'name'                       => $plural,
				'singular_name'              => $singular,
				'search_items'               => sprintf( __( 'Search %s', 'bitmovin' ), $plural ),
				'popular_items'              => sprintf( __( 'Popular %s', 'bitmovin' ), $plural ),
				'all_items'                  => sprintf( __( 'All %s', 'bitmovin' ), $plural ),
				'parent_item'                => sprintf( __( 'Parent %s', 'bitmovin' ), $singular ),
				'parent_item_colon'          => sprintf( __( 'Parent %s:', 'bitmovin' ), $singular ),
				'edit_item'                  => sprintf( __( 'Edit %s', 'bitmovin' ), $singular ),
				'update_item'                => sprintf( __( 'Update %s', 'bitmovin' ), $singular ),
				'add_new_item'               => sprintf( __( 'Add New %s', 'bitmovin' ), $singular ),
				'new_item_name'              => sprintf( __( 'New %s', 'bitmovin' ), $singular ),
				'separate_items_with_commas' => sprintf( __( 'Separate %s with commas', 'bitmovin' ), $plural ),
				'add_or_remove_items'        => sprintf( __( 'Add or remove %s', 'bitmovin' ), $plural ),
				'choose_from_most_used'      => sprintf( __( 'Choose from the most used %s', 'bitmovin' ), $plural ),
				'menu_name'                  => $plural,
			);

			$args = array(
				'labels'            => $labels,
				'public'            => true,
				'show_in_nav_menus' => false,
				'show_ui'           => true,
				'show_tagcloud'     => false,
				'show_admin_column' => true,
				'hierarchical'      => true,
				'rewrite'           => array( 'slug' => _x( 'variant-category', 'single-slug', 'bitmovin' ) ),
				'query_var'         => true
			);

			register_taxonomy( 'variant-category', array( 'variant' ), $args );
		}

		private function _register_taxonomy_variant_intensity() {

			$singular = __( 'Intensity', 'bitmovin' );
			$plural   = __( 'Intensities', 'bitmovin' );

			$labels = array(
				'name'                       => $plural,
				'singular_name'              => $singular,
				'search_items'               => sprintf( __( 'Search %s', 'bitmovin' ), $plural ),
				'popular_items'              => sprintf( __( 'Popular %s', 'bitmovin' ), $plural ),
				'all_items'                  => sprintf( __( 'All %s', 'bitmovin' ), $plural ),
				'parent_item'                => sprintf( __( 'Parent %s', 'bitmovin' ), $singular ),
				'parent_item_colon'          => sprintf( __( 'Parent %s:', 'bitmovin' ), $singular ),
				'edit_item'                  => sprintf( __( 'Edit %s', 'bitmovin' ), $singular ),
				'update_item'                => sprintf( __( 'Update %s', 'bitmovin' ), $singular ),
				'add_new_item'               => sprintf( __( 'Add New %s', 'bitmovin' ), $singular ),
				'new_item_name'              => sprintf( __( 'New %s', 'bitmovin' ), $singular ),
				'separate_items_with_commas' => sprintf( __( 'Separate %s with commas', 'bitmovin' ), $plural ),
				'add_or_remove_items'        => sprintf( __( 'Add or remove %s', 'bitmovin' ), $plural ),
				'choose_from_most_used'      => sprintf( __( 'Choose from the most used %s', 'bitmovin' ), $plural ),
				'menu_name'                  => $plural,
			);

			$args = array(
				'labels'            => $labels,
				'public'            => true,
				'show_in_nav_menus' => false,
				'show_ui'           => true,
				'show_tagcloud'     => false,
				'show_admin_column' => true,
				'hierarchical'      => true,
				'rewrite'           => array( 'slug' => _x( 'variant-intensity', 'single-slug', 'bitmovin' ) ),
				'query_var'         => true
			);

			register_taxonomy( 'variant-intensity', array( 'variant' ), $args );
		}
	}
}