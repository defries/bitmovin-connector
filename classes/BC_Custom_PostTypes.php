<?php
/*
 * All registrations for custom post types, have them made by a generator like on themergency.com and paste the functions in here, and have them run from: register_all_customposttypes()
 * 
 * @author Forsite Media
 */

if ( ! class_exists( 'BC_Custom_PostTypes' ) ) {

	class BC_Custom_PostTypes {

		public function __construct() {
			add_action( 'init', array( $this, 'register_all_customposttypes' ) );
		}

		public function register_all_customposttypes() {
			$this->_register_cpt_variant();
			$this->_register_cpt_encoding();
		}

		private function _register_cpt_encoding() {

			//only add this post type on blog ID we want to use for our encodings
			if ( get_current_blog_id() != BC_ENCODING_BLOG_ID ) {
				return;
			}

			$singular = __( 'Encoding', 'bitmovin' );
			$plural   = __( 'Encodings', 'bitmovin' );

			$labels = array(
				'name'               => $plural,
				'singular_name'      => $singular,
				'add_new'            => __( 'Add New', 'bitmovin' ),
				'add_new_item'       => sprintf( __( 'Add New %s', 'bitmovin' ), $singular ),
				'edit_item'          => sprintf( __( 'Edit %s', 'bitmovin' ), $singular ),
				'new_item'           => sprintf( __( 'New %s', 'bitmovin' ), $singular ),
				'view_item'          => sprintf( __( 'View %s', 'bitmovin' ), $singular ),
				'search_items'       => sprintf( __( 'Search %s', 'bitmovin' ), $plural ),
				'not_found'          => sprintf( __( 'No %s found', 'bitmovin' ), $plural ),
				'not_found_in_trash' => sprintf( __( 'No %s found in Trash', 'bitmovin' ), $plural ),
				'parent_item_colon'  => sprintf( __( 'Parent %s:', 'bitmovin' ), $singular ),
				'menu_name'          => $plural,
			);

			$args = array(
				'labels'              => $labels,
				'hierarchical'        => false,
				'supports'            => array(
					'title',
				),
				'public'              => false,
				'show_ui'             => true,
				'show_in_menu'        => true,
				'show_in_nav_menus'   => false,
				'publicly_queryable'  => false,
				'exclude_from_search' => false,
				'has_archive'         => false,
				'query_var'           => true,
				'can_export'          => false,
				'rewrite'             => false,
				'capability_type'     => 'page'
			);

			register_post_type( 'encoding', $args );
		}

		private function _register_cpt_variant() {

			$singular = __( 'Variant', 'bitmovin' );
			$plural   = __( 'Variants', 'bitmovin' );

			$labels = array(
				'name'               => $plural,
				'singular_name'      => $singular,
				'add_new'            => __( 'Add New', 'bitmovin' ),
				'add_new_item'       => sprintf( __( 'Add New %s', 'bitmovin' ), $singular ),
				'edit_item'          => sprintf( __( 'Edit %s', 'bitmovin' ), $singular ),
				'new_item'           => sprintf( __( 'New %s', 'bitmovin' ), $singular ),
				'view_item'          => sprintf( __( 'View %s', 'bitmovin' ), $singular ),
				'search_items'       => sprintf( __( 'Search %s', 'bitmovin' ), $plural ),
				'not_found'          => sprintf( __( 'No %s found', 'bitmovin' ), $plural ),
				'not_found_in_trash' => sprintf( __( 'No %s found in Trash', 'bitmovin' ), $plural ),
				'parent_item_colon'  => sprintf( __( 'Parent %s:', 'bitmovin' ), $singular ),
				'menu_name'          => $plural,
			);

			$args = array(
				'labels'              => $labels,
				'hierarchical'        => false,
				'supports'            => array(
					'title',
					'editor',
					'excerpt',
					'thumbnail',
					'genesis-cpt-archives-settings'
				),
				'public'              => true,
				'show_ui'             => true,
				'show_in_menu'        => true,
				'show_in_nav_menus'   => false,
				'publicly_queryable'  => true,
				'exclude_from_search' => false,
				'has_archive'         => _x( 'variants', 'archive-slug', 'bitmovin' ),
				'query_var'           => true,
				'can_export'          => true,
				'rewrite'             => array( 'slug' => _x( 'variant', 'single-slug', 'bitmovin' ) ),
				'capability_type'     => 'post'
			);

			register_post_type( 'variant', $args );
		}
	}
}