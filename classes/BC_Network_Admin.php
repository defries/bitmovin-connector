<?php

if ( ! class_exists( 'BC_Network_admin' ) ) {

	class BC_Network_Admin {

		public function __construct() {

			//actions
			add_action( 'network_admin_menu', array( $this, 'add_network_menu' ) );
		}

		//create a menu item
		public function add_network_menu() {

			add_menu_page( __( 'Bitmovin Connector', 'bitmovin' ), __( 'Bitmovin', 'bitmovin' ), 'administrator',
				'bitmovin', array( $this, 'add_network_menu_cb' ) );
		}

		//create the contents for the menu page
		public function add_network_menu_cb() {

			if ( ! is_super_admin() ) {
				wp_die( __( 'You do not have sufficient permissions to access this page.', 'bitmovin' ) );
			}

			//if any data was submitted, store it!
			if ( isset( $_POST['submit'] ) ) {

				$bitmovin_settings = array(
					'cron_recurrence'         => sanitize_title( $_POST['cron_recurrence'] ),
					'api_key_bitmovin'        => sanitize_text_field( $_POST['api_key_bitmovin'] ),
					'api_key_amazon_s3'       => sanitize_text_field( $_POST['api_key_amazon_s3'] ),
					'api_secret_amazon_s3'    => sanitize_text_field( $_POST['api_secret_amazon_s3'] ),
					'bucket_name_amazon_s3'   => sanitize_text_field( $_POST['bucket_name_amazon_s3'] ),
					'bucket_region_amazon_s3' => sanitize_title( $_POST['bucket_region_amazon_s3'] ),
					'bucket_prefix_amazon_s3' => trailingslashit( sanitize_text_field( $_POST['bucket_prefix_amazon_s3'] ) ),
					'sftp_host'               => sanitize_text_field( $_POST['sftp_host'] ),
					'sftp_username'           => sanitize_text_field( $_POST['sftp_username'] ),
					'sftp_password'           => sanitize_text_field( $_POST['sftp_password'] ),
					'sftp_path'               => trailingslashit( sanitize_text_field( $_POST['sftp_path'] ) ),
					'sftp_url'                => trailingslashit( esc_url( $_POST['sftp_url'] ) ),
					'drm_kid'                 => sanitize_text_field( $_POST['drm_kid'] ),
					'drm_key'                 => sanitize_text_field( $_POST['drm_key'] ),
					'widevine_pssh'           => sanitize_text_field( $_POST['widevine_pssh'] ),
					'playready_laurl'         => sanitize_text_field( esc_url( $_POST['playready_laurl'] ) ),
				);

				//save new data
				if ( update_site_option( 'bitmovin_settings', $bitmovin_settings ) ) {

					// (re)create the cronjob, when save was succesfull
					$this->_setup_cron( $bitmovin_settings['cron_recurrence'] );
				}
			}

			// if a request for an update was submitted, do it!
			if ( isset( $_POST['update'] ) ) {

				// update all the data now
				do_action( 'bc_update_event' );
			}

			if ( isset( $_POST['init-webhooks'] ) ) {

				// initialize webhooks for this site
				do_action( 'bc_init_webhooks' );
			}

			if ( isset( $_POST['unset-webhooks'] ) ) {

				// initialize webhooks for this site
				do_action( 'bc_unset_webhooks' );
			}

			//get the latest settings
			$bitmovin_settings = get_site_option( 'bitmovin_settings' );

			//create an array of empty values if nothing was found
			if ( ! $bitmovin_settings ) {

				$bitmovin_settings = array(
					'api_key_bitmovin'        => '',
					'cron_recurrence'         => '',
					'api_key_amazon_s3'       => '',
					'api_secret_amazon_s3'    => '',
					'bucket_name_amazon_s3'   => '',
					'bucket_region_amazon_s3' => '',
					'bucket_prefix_amazon_s3' => '',
					'sftp_host'               => '',
					'sftp_username'           => '',
					'sftp_password'           => '',
					'sftp_path'               => '',
					'sftp_url'                => '',
					'drm_kid'                 => '',
					'drm_key'                 => '',
					'widevine_pssh'           => '',
					'playready_laurl'         => '',
				);
			}

			//create html output for this page
			$html = '';

			$html .= '<div class="wrap">';
			$html .= '<form method="post" action="">';

			// Cronjob
			$html .= '<h2>' . __( 'Cronjob Settings', 'bitmovin' ) . '</h2>';
			//get available cron-schedules and add them to the select
			$possible_cron_schedules = wp_get_schedules();
			if ( is_array( $possible_cron_schedules ) && ( ! empty( $possible_cron_schedules ) ) ) {
				$html .= '<p>';
				$html .= __( 'Cron schedule', 'bitmovin' ) . '<br />';
				$html .= '<select name="cron_recurrence">';
				foreach ( $possible_cron_schedules as $key => $possible_cron_schedule ) {
					$html .= '<option ' . selected( $key, $bitmovin_settings['cron_recurrence'],
							false ) . ' value="' . esc_attr( $key ) . '">' . $possible_cron_schedule['display'] . '</option>';
				}

				$html .= '</select>';
				$html .= '</p>';
			}

			//Bitmovin
			$html .= '<h2>' . __( 'Bitmovin Settings', 'bitmovin' ) . '</h2>';
			$html .= '<p>';
			$html .= __( 'API Key', 'bitmovin' ) . '<br />';
			$html .= '<input type="text" name="api_key_bitmovin" value="' . esc_attr( $bitmovin_settings['api_key_bitmovin'] ) . '" size="80">';
			$html .= '</p>';

			//Amazon S3
			$html .= '<h2>' . __( 'Amazon S3 Settings', 'bitmovin' ) . '</h2>';
			$html .= '<p>';
			$html .= __( 'API Key', 'bitmovin' ) . '<br />';
			$html .= '<input type="text" name="api_key_amazon_s3" value="' . esc_attr( $bitmovin_settings['api_key_amazon_s3'] ) . '" size="80">';
			$html .= '</p>';
			$html .= '<p>';
			$html .= __( 'API Secret', 'bitmovin' ) . '<br />';
			$html .= '<input type="text" name="api_secret_amazon_s3" value="' . esc_attr( $bitmovin_settings['api_secret_amazon_s3'] ) . '" size="80">';
			$html .= '</p>';
			$html .= '<p>';
			$html .= __( 'Bucket name', 'bitmovin' ) . '<br />';
			$html .= '<input type="text" name="bucket_name_amazon_s3" value="' . esc_attr( $bitmovin_settings['bucket_name_amazon_s3'] ) . '" size="80">';
			$html .= '</p>';
			$html .= '<p>';
			$html .= __( 'Bucket region', 'bitmovin' ) . '<br />';
			$html .= '<input type="text" name="bucket_region_amazon_s3" value="' . esc_attr( $bitmovin_settings['bucket_region_amazon_s3'] ) . '" size="80">';
			$html .= '</p>';
			$html .= '<p>';
			$html .= __( 'Bucket prefix/folder', 'bitmovin' ) . '<br />';
			$html .= '<input type="text" name="bucket_prefix_amazon_s3" value="' . esc_attr( $bitmovin_settings['bucket_prefix_amazon_s3'] ) . '" size="80">';
			$html .= '</p>';

			//FTP for outputting the videos
			$html .= '<h2>' . __( 'FTP Output Settings', 'bitmovin' ) . '</h2>';
			$html .= '<p>';
			$html .= __( 'Host', 'bitmovin' ) . '<br />';
			$html .= '<input type="text" name="sftp_host" value="' . esc_attr( $bitmovin_settings['sftp_host'] ) . '" size="80">';
			$html .= '</p>';
			$html .= '<p>';
			$html .= __( 'Username', 'bitmovin' ) . '<br />';
			$html .= '<input type="text" name="sftp_username" value="' . esc_attr( $bitmovin_settings['sftp_username'] ) . '" size="80">';
			$html .= '</p>';
			$html .= '<p>';
			$html .= __( 'Password', 'bitmovin' ) . '<br />';
			$html .= '<input type="text" name="sftp_password" value="' . esc_attr( $bitmovin_settings['sftp_password'] ) . '" size="80">';
			$html .= '</p>';
			$html .= '<p>';
			$html .= __( 'Path/prefix', 'bitmovin' ) . '<br />';
			$html .= '<input type="text" name="sftp_path" value="' . esc_attr( $bitmovin_settings['sftp_path'] ) . '" size="80">';
			$html .= '</p>';
			$html .= '<p>';
			$html .= __( 'Public Base URL', 'bitmovin' ) . '<br />';
			$html .= '<input type="text" name="sftp_url" value="' . esc_attr( $bitmovin_settings['sftp_url'] ) . '" size="80">';
			$html .= '</p>';

			//DRM settings
			$html .= '<h2>' . __( 'DRM Settings', 'bitmovin' ) . '</h2>';
			$html .= '<p>';
			$html .= __( 'DRM Key ID', 'bitmovin' ) . '<br />';
			$html .= '<input type="text" name="drm_kid" value="' . esc_attr( $bitmovin_settings['drm_kid'] ) . '" size="80">';
			$html .= '</p>';
			$html .= '<p>';
			$html .= __( 'DRM Key', 'bitmovin' ) . '<br />';
			$html .= '<input type="text" name="drm_key" value="' . esc_attr( $bitmovin_settings['drm_key'] ) . '" size="80">';
			$html .= '</p>';
			$html .= '<p>';
			$html .= __( 'Widevine PSSH box', 'bitmovin' ) . '<br />';
			$html .= '<input type="text" name="widevine_pssh" value="' . esc_attr( $bitmovin_settings['widevine_pssh'] ) . '" size="80">';
			$html .= '</p>';
			$html .= '<p>';
			$html .= __( ' Playready license URL', 'bitmovin' ) . '<br />';
			$html .= '<input type="text" name="playready_laurl" value="' . esc_attr( $bitmovin_settings['playready_laurl'] ) . '" size="80">';
			$html .= '</p>';

			//submit/update
			$html .= '<hr />';
			$html .= '<p class="submit">';
			$html .= '<input type="submit" name="submit" class="button-primary" value="' . esc_attr( 'Save Changes',
					'bitmovin' ) . '" />';
			$html .= ' ';
			$html .= '<input type="submit" name="update" class="button-primary" value="' . esc_attr( 'Update now',
					'bitmovin' ) . '" />';
			$html .= ' ';

			$bitmovin_webhooks = get_site_option( 'bitmovin_webhooks' );
			if  ( isset( $bitmovin_webhooks ) && ( ! empty( $bitmovin_webhooks ) ) ) {
				//$html .= '<input type="submit" name="unset-webhooks" class="button-primary" value="' . esc_attr( 'Delete webhooks', 'bitmovin' ) . '" />';
			} else {
				//$html .= '<input type="submit" name="init-webhooks" class="button-primary" value="' . esc_attr( 'Initialize webhooks', 'bitmovin' ) . '" />';
			}

			$html .= '</p>';

			$html .= '</form>';
			$html .= '</div><!-- .wrap -->';

			echo $html;
		}

		/* Create a cron upon activation inside blog_id = 1, using the default (hourly) setting */
		private function _setup_cron( $recurrence = 'daily' ) {

			//clear previous setup, if any
			wp_clear_scheduled_hook( 'bc_update_event' );

			//add a new cron-event
			wp_schedule_event( time(), $recurrence, 'bc_update_event' );
		}
	}
}