<?php

if ( ! class_exists( 'BC_Custom_Meta_Boxes' ) ) {

	class BC_Custom_Meta_Boxes {

		/**
		 * Class constructor.
		 */
		public function __construct() {

			// Add video meta fields
			add_action( 'cmb2_init', array( $this, 'add_variant_metabox' ) );

			// Add encoding meta fields
			add_action( 'cmb2_init', array( $this, 'add_encoding_settings_metabox' ) );

			add_action( 'add_meta_boxes', array( $this, 'add_encoding_actions_metabox' ) );
		}

		public function add_encoding_actions_metabox() {
			add_meta_box( 'encoding-actions', __( 'Encoding actions', 'bitmovin' ),
				array( $this, 'add_encoding_actions_metabox_content' ), 'encoding' );
		}

		public function add_encoding_actions_metabox_content() {
			$html = '';

			if ( isset( $_GET['post'] ) ) {
				$job_id = get_post_meta( intval( $_GET['post'] ), '_job-id', true );
			}

			if ( isset( $job_id ) && ( ! empty( $job_id ) ) ) {
				$style = "display:none;";

				$encoding_status = get_post_meta( intval( $_GET['post'] ), '_encoding_status', true );
				if ( ! in_array( $encoding_status, array( 'FINISHED', 'ERROR' ) ) ) {
					$html .= '<div id="proceed-status-updates" style="display:none;"></div>';
				}
			} else {
				$style = "";
			}

			//add the button
			$html .= '<button class="button button-primary button-large" id="bitmovin-submit" style="' . esc_attr( $style ) . '" data-id="' . get_the_ID() . '">' . __( 'Submit encoding job',
					'bitmovin' ) . '</button>';

			$html .= '<div id="bitmovin-feedback">';
			if ( isset( $_GET['post'] ) ) {

				$messages = get_post_meta( intval( $_GET['post'] ), '_messages', true );
				if ( is_array( $messages ) && ( ! empty( $messages ) ) ) {
					foreach ( $messages as $message ) {
						$html .= '<p class="bitmovin-' . $message['type'] . '">' . $message['date'] . ' => ' . $message['text'] . ' </p>';
					}

				} else {
					$feedbacklines = BC_Custom_Functions::get_feedback_lines( intval( $_GET['post'] ) );
					if ( is_array( $feedbacklines ) ) {
						foreach ( $feedbacklines as $feedbackline ) {
							$html .= $feedbackline;
						}
					}
				}
			}
			$html .= '</div>';

			$html .= '<style> .bitmovin-ERROR { color: #ff0000;} </style>';

			echo $html;
		}


		public function add_encoding_settings_metabox() {

			/* Encoding metadata */
			$cmb2_box = new_cmb2_box( array(
				'id'           => 'encoding-meta',
				'title'        => __( 'Encoding specifications', 'bitmovin' ),
				'object_types' => array( 'encoding' ),
				'context'      => 'normal',
				'priority'     => 'high',
				'show_names'   => true,
			) );

			$cmb2_box->add_field( array(
				'name'             => __( 'Input video', 'bitmovin' ),
				'id'               => '_input',
				'type'             => 'select',
				'show_option_none' => true,
				'attributes'       => array(
					'data-validation' => 'required',
				),
				'options'          => BC_Custom_Functions::get_input_videos(),
			) );

			$cmb2_box->add_field( array(
				'name'              => __( 'Video encoding profiles', 'bitmovin' ),
				'id'                => '_encoding_profiles_video',
				'type'              => 'multicheck',
				'select_all_button' => true,
				'options'           => BC_Custom_Functions::get_encoding_profiles_video(),
			) );

			$cmb2_box->add_field( array(
				'name'              => __( 'Audio encoding profiles', 'bitmovin' ),
				'id'                => '_encoding_profiles_audio',
				'type'              => 'multicheck',
				'select_all_button' => true,
				'options'           => BC_Custom_Functions::get_encoding_profiles_audio(),
			) );

			$cmb2_box->add_field( array(
				'name'              => __( 'Adaptive Streaming Formats ', 'bitmovin' ),
				'id'                => '_adaptive_streaming_formats',
				'type'              => 'multicheck',
				'select_all_button' => true,
				'options'           => BC_Custom_Functions::get_adaptive_streaming_formats(),
			) );
		}

		/**
		 * Add meta boxes.
		 */
		public function add_variant_metabox() {

			/* Video metadata */
			$cmb2_box = new_cmb2_box( array(
				'id'           => 'variant-meta',
				'title'        => __( 'Variant metadata', 'bitmovin' ),
				'object_types' => array( 'variant' ), // Post type
				'context'      => 'normal',
				'priority'     => 'high',
				'show_names'   => true, // Show field names on the left
			) );

			$cmb2_box->add_field( array(
				'name' => __( 'Releasedate', 'bitmovin' ),
				'id'   => '_releasedate',
				'type' => 'text_date_timestamp',
			) );

			$cmb2_box->add_field( array(
				'name' => __( 'Recordingdate', 'bitmovin' ),
				'id'   => '_recorddate',
				'type' => 'text_date_timestamp',
			) );

			$cmb2_box->add_field( array(
				'name' => __( 'kCal', 'bitmovin' ),
				'id'   => '_kcal',
				'type' => 'text',
			) );

			$cmb2_box->add_field( array(
				'name' => __( 'Variant name', 'bitmovin' ),
				'desc' => __( 'Example: Full/Regular or CutDown', 'bitmovin' ),
				'id'   => '_variant_name',
				'type' => 'text',
			) );

			$cmb2_box->add_field( array(
				'name' => __( 'Specific lesson name', 'bitmovin' ),
				'desc' => __( 'This setting will override the default lesson name', 'bitmovin' ),
				'id'   => '_specific_lesson_name',
				'type' => 'text',
			) );

			$cmb2_box->add_field( array(
				'name'             => __( 'Video file', 'bitmovin' ),
				'id'               => '_video_file_main',
				'type'             => 'select',
				'show_option_none' => true,
				'options'          => BC_Custom_Functions::get_available_manifests(),
			) );

			$cmb2_box->add_field( array(
				'name'             => __( 'Preview video file', 'bitmovin' ),
				'id'               => '_video_file_preview',
				'type'             => 'select',
				'show_option_none' => true,
				'options'          => BC_Custom_Functions::get_available_manifests(),
			) );

			$cmb2_box->add_field( array(
				'name'             => __( 'Required subscription', 'bitmovin' ),
				'id'               => '_required_subscriptions',
				'type'             => 'select',
				'show_option_none' => true,
				'options'          => $this->_get_subscription_options(),
			) );

			$cmb2_box->add_field( array(
				'name' => __( 'Internal notes', 'bitmovin' ),
				'id'   => '_notes',
				'type' => 'textarea',
			) );
		}

		private function _get_subscription_options() {
			return array(
				'standard' => __( 'Subscription One', 'bitmovin' ),
				'custom'   => __( 'Subscription Two', 'bitmovin' ),
				'none'     => __( 'Subscription Three', 'bitmovin' ),
			);
		}
	}
}
